package com.example.storytel;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class RemoteBooking {

    public String bookABook(String bookId) throws BookingClientException {
        try {
            WebClient webClient = WebClient.create("http://localhost:" + 8081 + "/booking/"+bookId);

            return webClient.get()
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
        } catch (Exception e) {
            throw new BookingClientException(e);
        }
    }
    public Mono<String> bookABook2(String bookId)  {
        return WebClient.create("http://localhost:" + 8081 + "/booking/"+bookId)
                .get()
                .retrieve()
                .bodyToMono(String.class);
    }
}
