package com.example.storytel;

public class BookingClientException extends Exception{
    public BookingClientException(Throwable cause) {
        super(cause);
    }
}
