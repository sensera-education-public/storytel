package com.example.storytel;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class BookService {

    BookRepository bookRepository;
    RemoteBooking remoteBooking;

    public BookEntity bookABook(String bookId) throws BookAlreadyBookedException, BookingClientException {
        BookEntity book = bookRepository.getById(bookId);
        if (book.getBookingId() != null)
            throw new BookAlreadyBookedException();

        String bookingId = remoteBooking.bookABook(bookId);

        book.setBookingId(bookingId);

        return bookRepository.save(book);
    }

    public Mono<BookEntity> bookABook2(String bookId) {
        return Mono.just(bookId)
                .map(s -> bookRepository.getById(bookId))
                .flatMap(bookEntity -> remoteBooking.bookABook2(bookId)
                        .map(bookingId -> {
                            bookEntity.setBookingId(bookingId);
                            return bookEntity;
                        }))
                .doOnNext(o -> bookRepository.save(o));
    }
}
