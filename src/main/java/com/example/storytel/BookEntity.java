package com.example.storytel;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity(name = "book")
@NoArgsConstructor
@Data
public class BookEntity {
    @Id String id;

    @Column(name = "title")
    String title;

    @Column(name = "stream_id")
    String streamId;

    @Column(name = "description")
    String description;

    @Column(name = "booking_id")
    String bookingId;

    public BookEntity(String title, String streamId, String description) {
        this.id = UUID.randomUUID().toString();
        this.title = title;
        this.streamId = streamId;
        this.description = description;
        this.bookingId = null;
    }
}
