package com.example.storytel;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.stream.Stream;

public interface BookRepository extends JpaRepository<BookEntity, String> {
    List<BookEntity> findBookEntitiesByTitleContaining(String title);
    List<BookEntity> findBookEntitiesByDescriptionContaining(String description);
    List<BookEntity> findBookEntitiesByTitleContainingOrDescriptionContaining(String title, String description);
}
