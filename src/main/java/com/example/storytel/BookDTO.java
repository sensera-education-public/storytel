package com.example.storytel;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class BookDTO {
    String id;
    String title;
    String streamId;
    String description;
    String bookingId;

    @JsonCreator
    public BookDTO(@JsonProperty("id") String id,
                   @JsonProperty("title") String title,
                   @JsonProperty("streamId") String streamId,
                   @JsonProperty("description") String description,
                   @JsonProperty("bookingId") String bookingId) {
        this.id = id;
        this.title = title;
        this.streamId = streamId;
        this.description = description;
        this.bookingId = bookingId;
    }
}
