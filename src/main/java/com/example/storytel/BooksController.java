package com.example.storytel;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("books")
@AllArgsConstructor
public class BooksController {

    BookRepository bookRepository;
    BookService bookService;

    @GetMapping()
    @Transactional
    public List<BookDTO> all(@RequestParam(value = "titel", required = false) String titel, @RequestParam(value = "description", required = false) String description) {
        if (titel != null) {
            if (description != null)
                return bookRepository.findBookEntitiesByTitleContainingOrDescriptionContaining(titel, description).stream().map(BooksController::toDTO).collect(Collectors.toList());
            return bookRepository.findBookEntitiesByTitleContaining(titel).stream().map(BooksController::toDTO).collect(Collectors.toList());
        }
        if (description != null)
            return bookRepository.findBookEntitiesByDescriptionContaining(description).stream().map(BooksController::toDTO).collect(Collectors.toList());
        return bookRepository.findAll().stream().map(BooksController::toDTO).collect(Collectors.toList());
    }

    @GetMapping("/{bookId}/bookABook")
    @Transactional
    public Mono<ResponseEntity<BookDTO>> bookABook2(@PathVariable("bookId") String bookId) {
        return bookService.bookABook2(bookId)
                .map(BooksController::toDTO)
                .map(ResponseEntity::ok);
    }

    @GetMapping("/{bookId}/bookABook2")
    @Transactional
    public Mono<ResponseEntity<BookDTO>> bookABook(@PathVariable("bookId") String bookId) {
        try {
            return Mono.just(ResponseEntity.ok(toDTO(bookService.bookABook(bookId))));
        } catch (BookAlreadyBookedException e) {
            return Mono.just(ResponseEntity.badRequest().build());
        } catch (BookingClientException e) {
            return Mono.just(ResponseEntity.status(502).build());
        }
    }

    public static BookDTO toDTO(BookEntity bookEntity) {
        return new BookDTO(
                bookEntity.getId(),
                bookEntity.getTitle(),
                bookEntity.getStreamId(),
                bookEntity.getDescription(),
                bookEntity.getBookingId()
        );
    }

}
