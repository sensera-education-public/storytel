package com.example.storytel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("test")
public class BooksControllerLoadTest {
    static int count = 10000;

    @LocalServerPort
    int port;

    @Autowired
    BookRepository bookRepository;

    List<String> bookIdList;

    @BeforeEach
    @Transactional
    void setUp() {
        bookRepository.deleteAll();
        bookIdList = IntStream.range(0, count).boxed().map(no -> {
                    BookEntity bookEntity1 = new BookEntity("Web "+no, ""+no, "Grymt bra bok.");
                    bookRepository.save(bookEntity1);
                    return bookEntity1.getId();
                })
                .collect(Collectors.toList());
    }

    @AfterEach
    @Transactional
    void tearDown() {
        bookRepository.deleteAll();
    }

    @Test
    @Disabled
    void test_book_a_book_success() throws BookingClientException {
        long start = System.currentTimeMillis();

        AtomicLong sum = new AtomicLong();
        List<String> bookings = bookIdList.stream()
                .parallel()
                .map(BookingClient::new)
                .peek(BookingClient::bookABook)
                .peek(bookingClient -> sum.addAndGet(bookingClient.duration))
                .map(BookingClient::getAsyncBookingId)
                .collect(Collectors.toList());

        long duration = System.currentTimeMillis() - start;
        System.out.println("Took "+duration+"msec");
        System.out.println("Took average "+duration/count+"msec per booking");
        System.out.println("Total call duration "+sum.get());
        System.out.println("Total average call duration "+sum.get()/count);
    }


    private class BookingClient {
        String bookId;
        String bookingId;
        long duration;

        public BookingClient(String bookId) {
            this.bookId = bookId;
        }

        private String bookABook() {
            WebClient webClient = WebClient.create("http://localhost:" + port + "/books/" + bookId + "/bookABook");

            long start = System.currentTimeMillis();
            String bookingId = webClient.get()
                    .retrieve()
                    .bodyToMono(BookDTO.class)
                    .block().getBookingId();

            duration = System.currentTimeMillis() - start;
            synchronized (this) {
                this.bookingId = bookingId;
                this.notifyAll();
            }

            return bookingId;
        }

        String getAsyncBookingId() {
            synchronized (this) {
                if (this.bookingId!=null)
                    return bookingId;
                try {
                    this.wait(30000);
                } catch (InterruptedException e) {throw new RuntimeException("Interrupted ",e);}
                if (this.bookingId == null)
                    throw new RuntimeException("Timeout!");
                return bookingId;
            }
        }
    }

}
