package com.example.storytel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BooksControllerTestWithMocking {

    @LocalServerPort int port;

    @MockBean BookRepository bookRepository;
    @MockBean BookService bookService;

    BookEntity bookEntity1;
    BookEntity bookEntity2;
    BookDTO book1;
    BookDTO book2;

    @BeforeEach
    @Transactional
    void setUp() {
        bookEntity1 = new BookEntity("Web 1", "1", "Grymt bra bok.");
        bookEntity2 = new BookEntity("Web 2", "2", "Handlar om något spännande!");

        book1 = BooksController.toDTO(bookEntity1);
        book2 = BooksController.toDTO(bookEntity2);
    }

    @AfterEach
    void tearDown() {
        bookRepository.deleteAll();
    }

    @Test
    void test_get_all_books_success() {
        when(bookRepository.findAll()).thenReturn(List.of(bookEntity1, bookEntity2));

        List<BookDTO> books = getBooks("");

        assertEquals(List.of(book1, book2), books);
    }

    @Test
    void test_list_books_filter_by_title_success() {
        when(bookRepository.findBookEntitiesByTitleContaining(eq("Web 1"))).thenReturn(List.of(bookEntity1));

        List<BookDTO> books = getBooks("?titel=Web 1");

        assertEquals(List.of(book1), books);
    }

    @Test
    void test_list_books_filter_by_description_success() {
        when(bookRepository.findBookEntitiesByDescriptionContaining(eq("något"))).thenReturn(List.of(bookEntity2));

        List<BookDTO> books = getBooks("?description=något");

        assertEquals(List.of(book2), books);
    }

    @Test
    void test_book_a_book_fail_because_already_booked() throws BookingClientException, BookAlreadyBookedException {
        when(bookService.bookABook(eq("123345"))).thenThrow(new BookAlreadyBookedException());

        WebClient webClient = WebClient.create("http://localhost:" + port + "/books/123345/bookABook" );

        WebClientResponseException webClientResponseException = assertThrows(WebClientResponseException.class, () -> {
            webClient.get()
                    .retrieve()
                    .bodyToMono(BookDTO.class)
                    .block();
        });

        assertEquals(400, webClientResponseException.getRawStatusCode());
    }


    private List<BookDTO> getBooks(String url) {
        WebClient webClient = WebClient.create("http://localhost:" + port + "/books" + url);

        return webClient.get()
                .retrieve()
                .bodyToFlux(BookDTO.class)
                .collectList()
                .block();
    }

}
