package com.example.storytel;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class BookServiceTest {

    @Autowired BookService bookService;
    @Autowired BookRepository bookRepository;

    @MockBean RemoteBooking remoteBooking;

    @Test
    @Transactional
    void test_book_a_book_success() throws BookAlreadyBookedException, BookingClientException {
        String bookId = bookRepository.save(new BookEntity("Web 1", "1", "Grymt bra bok.")).getId();
        when(remoteBooking.bookABook(eq(bookId))).thenReturn("1234567");

        BookEntity bookEntity = bookService.bookABook(bookId);

        assertEquals("1234567", bookEntity.getBookingId());
    }

    @Test
    @Transactional
    void test_book_a_book_fail_because_already_booked() {
        BookEntity entity = new BookEntity("Web 1", "1", "Grymt bra bok.");
        entity.setBookingId("2348279487");
        String bookId = bookRepository.save(entity).getId();

        BookAlreadyBookedException bookAlreadyBookedException = assertThrows(BookAlreadyBookedException.class, () -> bookService.bookABook(bookId));

        assertNotNull(bookAlreadyBookedException);
    }

    @Test
    @Transactional
    void test_book_a_book_fail_because_remote_api_error() throws BookingClientException {
        String bookId = bookRepository.save(new BookEntity("Web 1", "1", "Grymt bra bok.")).getId();
        when(remoteBooking.bookABook(eq(bookId))).thenThrow(new BookingClientException(null));

        BookingClientException bookingClientException = assertThrows(BookingClientException.class, () -> bookService.bookABook(bookId));

        assertNotNull(bookingClientException);
    }
}
