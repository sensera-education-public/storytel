package com.example.storytel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import javax.transaction.Transactional;
import java.net.URL;
import java.net.http.HttpClient;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BooksControllerTest {

    @LocalServerPort int port;

    @Autowired BookRepository bookRepository;

    @MockBean RemoteBooking remoteBooking;

    BookDTO book1;
    BookDTO book2;

    @BeforeEach
    @Transactional
    void setUp() {
        BookEntity bookEntity1 = new BookEntity("Web 1", "1", "Grymt bra bok.");
        BookEntity bookEntity2 = new BookEntity("Web 2", "2", "Handlar om något spännande!");
        bookRepository.save(bookEntity1);
        bookRepository.save(bookEntity2);

        book1 = BooksController.toDTO(bookEntity1);
        book2 = BooksController.toDTO(bookEntity2);
    }

    @AfterEach
    void tearDown() {
        bookRepository.deleteAll();
    }

    @Test
    void test_get_all_books_success() {
        List<BookDTO> books = getBooks("");

        assertEquals(List.of(book1, book2), books);
    }

    @Test
    void test_list_books_filter_by_title_success() {
        List<BookDTO> books = getBooks("?titel=Web 1");

        assertEquals(List.of(book1), books);
    }

    @Test
    void test_list_books_filter_by_description_success() {
        List<BookDTO> books = getBooks("?description=något");

        assertEquals(List.of(book2), books);
    }

    @Test
    void test_book_a_book_success() throws BookingClientException {
        WebClient webClient = WebClient.create("http://localhost:" + port + "/books/"+book1.getId()+"/bookABook" );
        when(remoteBooking.bookABook(eq(book1.getId()))).thenReturn("1234567");

        BookDTO book =  webClient.get()
                .retrieve()
                .bodyToMono(BookDTO.class)
                .block();

        assertEquals("1234567", book.getBookingId());
    }

    @Test
    void test_book_a_book_fail_because_already_booked() throws BookingClientException {
        BookEntity bookEntity3 = new BookEntity("Web 3", "3", "Grymt bra bok.");
        bookEntity3.setBookingId("1234567");
        bookRepository.save(bookEntity3);
        WebClient webClient = WebClient.create("http://localhost:" + port + "/books/"+bookEntity3.getId()+"/bookABook" );

        WebClientResponseException webClientResponseException = assertThrows(WebClientResponseException.class, () -> {
            webClient.get()
                    .retrieve()
                    .bodyToMono(BookDTO.class)
                    .block();
        });

        assertEquals(400, webClientResponseException.getRawStatusCode());
    }

    private List<BookDTO> getBooks(String url) {
        WebClient webClient = WebClient.create("http://localhost:" + port + "/books" + url);

        return webClient.get()
                .retrieve()
                .bodyToFlux(BookDTO.class)
                .collectList()
                .block();
    }



}
